
%include F31-GAMES-x86_64-repo-mirrorlist.ks
%include F31-GAMES-x86_64-rpmfusion-free-repo-mirrorlist.ks
%include F31-GAMES-x86_64-rpmfusion-nonfree-repo-mirrorlist.ks

%include F31-GAMES-x86_64-rpmfusion-free-live-base.ks
%include F31-GAMES-x86_64-rpmfusion-nonfree-live-base.ks

%include F31-GAMES-x86_64-live-xfce.ks


%include F31-GAMES-x86_64-games.ks



%packages


firefox
mozilla-https-everywhere
#mozilla-noscript
mozilla-ublock-origin

pidgin
#pidgin-otr
pidgin-discord

#java-11-openjdk


# who doesn't love a dancing hotdog

plymouth-theme-hot-dog


%end

%post

echo "** Postinstall: "
echo "* Disable unneeded services"

cat >> /README.TXT << FOE
Welcome!

su -

systemctl stop firewalld

setenforce Permissive

# Teeworlds Server:

teeworlds-srv -f teeworlds-dm.cfg




FOE

cat >> /teeworlds-dm.cfg  << FOE

sv_name LAN
bindaddr
sv_port 8303
sv_external_port 0
sv_max_clients 12
sv_max_clients_per_ip 12
sv_high_bandwidth 0
sv_register 1 #change to 1 for lan 
sv_map dm1
sv_rcon_password lug4life
password 
logfile 
console_output_file 0
sv_rcon_max_tries 3
sv_rcon_bantime 5
sv_auto_demo_record 0 
sv_auto_demo_max 10
ec_bindaddr localhost
ec_bantime 0
ec_auth_timeout 30
ec_output_level 1

#Game Settings 
sv_warmup 0
sv_scorelimit 20
sv_gametype dm
sv_maprotation
sv_rounds_per_map 1
sv_motd Welcome to our server!
sv_player_slots 8
sv_teambalance_time
sv_spamprotection 1
sv_tournament_mode 0
sv_player_ready_mode 0
sv_strict_spectate_mode 0
sv_silent_spectator_mode 0
sv_skill_level 0
sv_respawn_delay_tdm 3
sv_vote_kick 1
sv_vote_kick_bantime 5
sv_vote_kick_min 0
sv_inactivekick_time 3
sv_inactivekick 1
sv_vote_spectate 1
sv_vote_spectate_rejoindelay 3

FOE




cat >> /root/README.TXT << FOE
Welcome Root!

FOE


#mkdir -p /home/liveuser/Desktop/
#cat >> /home/liveuser/Desktop/README.TXT << FOE
#Welcome LiveUser!

#FOE

# this goes at the end after all other changes. 
#chown -R liveuser:liveuser /home/liveuser
#restorecon -R /home/liveuser


echo "** Done!"
%end

%post
echo F31-GAMES-x86_64-$(date +%F) > /etc/live-release
%end
%post
echo F31-GAMES-x86_64-2019-12-09-1705 > /etc/live-release
%end
