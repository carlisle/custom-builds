## reminder: enable and disable as well as modify following repos to make sure
## the repos match what is configured in fedora-live-base.ks

# To compose against the current release tree, use the following "repo"
###repo --name=rpmfusion-free-released --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-$releasever&arch=$basearch

# To include updates, use the following "repo"
###repo --name=rpmfusion-free-updates --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-updates-released-$releasever&arch=$basearch

# To compose against rawhide, use the following "repo"
#repo --name=rpmfusion-free-rawhide --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-rawhide&arch=$basearch

# Make sure you read /usr/share/doc/rpmfusion-nonfree-remix-kickstarts*/README 
# before distributing a linux distribution that is build with this kickstart file
#
# Reminder: The RPM Fusion Nonfree repos depend on the Free repos; so you should
# include /usr/share/rpmfusion-free-remix-kickstarts/rpmfusion-free-live-base.ks
# directly or indirectly via some other "live" kickstart file from the package
# rpmfusion-free-remix-kickstarts in any kickstart files that include this file.



# Please note that the repos are configured to just track in 
# rpmfusion-nonfree-release and nothing else
#
## Reminder: enable and disable as well as modify following repos to make sure
## the repos match what is configured in fedora-live-base.ks


# To compose against the current release tree, use the following "repo"
#repo --name=rpmfusion-nonfree --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-$releasever&arch=$basearch  --includepkgs=rpmfusion-nonfree-release --includepkgs=discord

#repo --name=rpmfusion-nonfree --metalink=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-$releasever&arch=$basearch
repo --name=rpmfusion-nonfree --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-$releasever&arch=$basearch"
#repo --name=rpmfusion-nonfree --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-$releasever&arch=$basearch"


# To include updates, use the following repo
#repo --name=rpmfusion-nonfree-updates --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch  --includepkgs=rpmfusion-nonfree-release --includepkgs=discord
#repo --name=rpmfusion-nonfree-updates --metalink=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch
repo --name=rpmfusion-nonfree-updates --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch"
#repo --name=rpmfusion-nonfree-updates --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch"


# To include tainted packages, use the following repo

repo --name=rpmfusion-nonfree-tainted --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-tainted-$releasever&arch=$basearch"
#repo --name=rpmfusion-nonfree-tainted --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-tainted-$releasever&arch=$basearch"


# To compose against rawhide, use the following "repo"
#repo --name=rpmfusion-nonfree-rawhide --mirrorlist=https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-rawhide&arch=$basearch --includepkgs=rpmfusion-nonfree-release



