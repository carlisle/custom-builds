#!/bin/env bash

# this script must be run as root
# Verify system is up to date before building iso

# required packages:
# fedora-kickstarts
# rpmfusion-free-remix-kickstarts
# rpmfusion-nonfree-remix-kickstarts
# livecd-tools
# livecd-iso-to-mediums
# dc3dd
# coreutils

# kickstart files
# fedora-live-base.ks
# fedora-live-minimization.ks
# fedora-live-xfce.ks
# fedora-repo.ks
# fedora-repo-not-rawhide.ks
# fedora-xfce-common.ks



# Start timer
Timer_Start=$(date +%s)
NTimer_Start=$(date +%s.%N)

DISTRO='fedora'
SPIN='xfce'
LABEL=${DISTRO}-${SPIN}
ARCH='x86_64'
BASE="/home/carlisle/Livecd"

#CreatorTmp=${BASE}/tmp
CreatorTmp=/tmp/carlisle-Livecd
CreatorCache=${BASE}/cache

SHORTDATE=$(date +%d%b%y)
LONGDATE=$(date +%F_%Hh%Mm)


FSLABEL=${LABEL}-${ARCH}-Live-${SHORTDATE}

# verify tmp and cache exist
if [ ! -d "${CreatorTmp}"   ]; then mkdir -p ${CreatorTmp}; fi
if [ ! -d "${CreatorCache}" ]; then mkdir -p ${CreatorCache}; fi
mkdir ${BASE}/${LABEL}-${LONGDATE} 
cd    ${BASE}/${LABEL}-${LONGDATE}

touch ${FSLABEL}

ls ../${DISTRO}-${SPIN}*.ks

declare -a KS_Array=(
  ${LABEL}-${ARCH}-repo-mirrorlist.ks
  ${LABEL}-${ARCH}-rpmfusion-free-repo-mirrorlist.ks
  ${LABEL}-${ARCH}-rpmfusion-nonfree-repo-mirrorlist.ks
  ${LABEL}-${ARCH}-xfce-common.ks
  ${LABEL}-${ARCH}-live-minimization.ks
  ${LABEL}-${ARCH}-live-base.ks
  ${LABEL}-${ARCH}-live-xfce.ks
  ${LABEL}-${ARCH}-rpmfusion-free-live-base.ks
  ${LABEL}-${ARCH}-rpmfusion-nonfree-live-base.ks
#  ${LABEL}-${ARCH}-rpmfusion-nonfree-live-nvidia.ks
  ${LABEL}-${ARCH}-games.ks
  ${LABEL}-${ARCH}-livecd.ks
  create${LABEL}-${ARCH}.sh
)

# Copy all files to build directory
for KS_File in "${KS_Array[@]}"; do
  cp -p ../${KS_File} .
done

printf "%s\n" "%post" >> ./${LABEL}-${ARCH}-livecd.ks
printf "%s\n" "echo ${LABEL}-${ARCH}-${LONGDATE} > /etc/live-release" >> ./${LAB
EL}-${ARCH}-livecd.ks
printf "%s\n" "%end" >> ./${LABEL}-${ARCH}-livecd.ks

printf "%s\n" "Built on : ${LONGDATE}" > livecd-creator-log.txt
printf "%s" "Built with: " >> livecd-creator-log.txt
rpm -q livecd-tools >> livecd-creator-log.txt

/usr/bin/livecd-creator \
 --config=./${LABEL}-${ARCH}-livecd.ks  \
 --fslabel=${FSLABEL} \
 --tmpdir=${CreatorTmp} \
 --cache=${CreatorCache}/${DISTRO}-${ARCH} \
 2>&1 | tee livecd-creator-log.txt

cat > readme.txt <<EOF
To verify these files under linux use either:
md5sum  --check md5sum-checksum.txt
sha1sum --check sha1sum-checksum.txt

To run as a virtual machine under linux use:
SDL_VIDEO_X11_DGAMOUSE=0 qemu-kvm -m 1024 -cdrom ./${FSLABEL}.iso

examples of writing iso to usb drive:
dc3dd if=./${LABEL}-${LONGDATE}/${FSLABEL}.iso of=/dev/sdX
To create a persistent overlay or encrypted home directory use:
livecd-iso-to-disk --overlay-size-mb 512 ./${LABEL}-${LONGDATE}/${FSLABEL}.iso /dev/sdX1
livecd-iso-to-disk --overlay-size-mb 256 --home-size-mb 256 ./${LABEL}-${LONGDATE}/${FSLABEL}.iso /dev/sdX1

or windows tool: https://fedorahosted.org/liveusb-creator
see: https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/index.html
see: https://fedoraproject.org/wiki/FedoraLiveCD


EOF


# End timer

Timer_End=$(date +%s)
NTimer_End=$(date +%s.%N)

let Timer_Total="${Timer_End} - ${Timer_Start}"
NTimer_Total=$(echo "${NTimer_End} - ${NTimer_Start}" | bc)

printf "%s\n" "Script took ${Timer_Total} seconds"
printf "%s\n" "Script took ${Timer_Total} seconds" >> readme.txt
printf "%s\n" "Script tool ${NTimer_Total} nanoseconds"
printf "%s\n" "Script tool ${NTimer_Total} nanoseconds" >> readme.txt


# Write checksum files for given hash functions
declare -a Hash_Array=( md5sum sha1sum )

for Hash_Func in "${Hash_Array[@]}"; do

  Hash_File=${Hash_Func}-checksums.txt
  touch ${Hash_File}

  printf "%s\n" "# ${LABEL}-${ARCH}-${LONGDATE} ${Hash_Func} Checksums: #" > ${Hash_File}

  ${Hash_Func} ${FSLABEL}.iso >> ${Hash_File}
  ${Hash_Func} create${LABEL}-${ARCH}.sh >> ${Hash_File}

  for KS_File in "${KS_Array[@]}"; do
    ${Hash_Func} ${KS_File} >> ${Hash_File}
  done

  ${Hash_Func} livecd-creator-log.txt >>  ${Hash_File}
  ${Hash_Func} readme.txt  >>  ${Hash_File}

done

cat readme.txt

