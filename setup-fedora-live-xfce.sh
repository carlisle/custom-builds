cd <build directory>

ln -s /usr/share/spin-kickstarts 
ln -s /usr/share/rpmfusion-free-remix-kickstarts
ln -s /usr/share/rpmfusion-nonfree-remix-kickstarts

# symlink to the primary kickstart file
ln -s ./spin-kickstarts/fedora-live-xfce.ks 

# determine dependecies by search for include statements
# grep ^%include fedora-live-xfce.ks 
# then symlink to those 

ln -s ./spin-kickstarts/fedora-live-base.ks 
ln -s ./spin-kickstarts/fedora-live-minimization.ks 
ln -s ./spin-kickstarts/fedora-xfce-common.ks 
ln -s ./spin-kickstarts/fedora-repo.ks 
ln -s ./spin-kickstarts/fedora-repo-not-rawhide.ks 

# show all files
ls -l

