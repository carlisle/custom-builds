url --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
#url --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-31&arch=x86_64"

repo --name=fedora --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch"
#repo --name=fedora  --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch"

repo --name=fedora-modular  --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-modular-$releasever&arch=$basearch"
#repo --name=fedora-modular  --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-modular-$releasever&arch=$basearch"

repo --name=updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f$releasever&arch=$basearch"
#repo --name=updates  --metalink="https://mirrors.fedoraproject.org/metalink?repo=updates-released-f$releasever&arch=$basearch"

repo --name=updates-modular --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-modular-f$releasever&arch=$basearch"
#repo --name=updates-modular --metalink="https://mirrors.fedoraproject.org/metalink?repo=updates-released-modular-f$releasever&arch=$basearch"

#repo --name=updates-testing --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=updates-testing-f$releasever&arch=$basearch

#repo --name=rawhide --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=rawhide&arch=$basearch
#url --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=rawhide&arch=$basearch

